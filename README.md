#Unity Tech Test

## Your Task

**You have been given a Rock Paper Scissors game prototype. Add features of your choice, restructure, refactor and fix any bugs! Change whatever you want. Do what you think is the best representation of your skillset but please do not focus on the UI.**

* Use any online resource for help, including Unity documentation and C#. 
* Your work should be your original work and not copy/paste examples from Stackoverflow or other online resources.
* If you end up using *any* third part code, this must be clearly stated in a comment with source. 

## Deliverables

* Email back the updated project either in a new zip file or include a github repo link.
* Include in your finished project any notes you may have.
* In the body of your email, please explain the areas you focused on, improvements you've made, and any other information you feel is valuable.
