﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//I Avoid singletons, but for this it makes sense;
public class DataManager : Singleton<DataManager>
{
    //Raw game data
    public GameData CurrentGameData { get; private set; }
    public PlayerData CurrentPlayerData { get; private set; }

    //Efficient and convenient way to access the gestures;
    public Dictionary<string, GestureData> AvailableGestures { get; private set; }

    private IServerInterface serverInterface;

    public void RequestPlay(PlayRequestData playRequestData, Action<PlayData> OnSuccess, Action<string> OnError)
    {
        StartCoroutine(serverInterface.RequestPlay(playRequestData, (playdata) => UpdatePlayerData(playdata, OnSuccess), OnError));
    }

    public void UpdatePlayerData(PlayData playData, Action<PlayData> OnSuccess)
    {
        CurrentPlayerData.Coins = playData.PlayerCoins;
        OnSuccess(playData);
    }

    protected override void Awake()
    {
        base.Awake();
        serverInterface = GetComponent<IServerInterface>();
        GameFlowController.GameStateChanged += GameFlowController_StateChangedHandler;
    }

    /// <summary>
    /// When the game is on Loading state, fetch data from server.
    /// </summary>
    /// <param name="gameState"></param>
    private void GameFlowController_StateChangedHandler(eGameState gameState)
    {
        if (gameState != eGameState.Loading || serverInterface == null)
        {
            return;
        }

        CurrentGameData = null;
        StartCoroutine(serverInterface.RequestGameData(
            (gameData) =>
            {
                AvailableGestures = new Dictionary<string, GestureData>();
                foreach (var gesture in gameData.HandGestures)
                {
                    AvailableGestures.Add(gesture.GestureID, gesture);
                }
                CurrentGameData = gameData;
            },
            (error) =>
            {
                Debug.LogError(error);
            }));

        CurrentPlayerData = null;
        StartCoroutine(serverInterface.RequestPlayerData(
        "John Doe",
        (playerData) =>
        {
            CurrentPlayerData = playerData;
        },
        (error) =>
        {
            Debug.LogError(error);
        }));
    }

    private void OnDestroy()
    {
        GameFlowController.GameStateChanged -= GameFlowController_StateChangedHandler;
    }
}
