﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The interface that the server (or mock) must implement.
/// </summary>
public interface IServerInterface
{
    IEnumerator RequestGameData(Action<GameData> OnSuccess, Action<string> OnError);
    IEnumerator RequestPlayerData(string playerID, Action<PlayerData> OnSuccess, Action<string> OnError);
    IEnumerator RequestPlay(PlayRequestData playRequestData, Action<PlayData> OnSuccess, Action<string> OnError);
}
