﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This User button class is used to automatically sign its unity button to our User Input Controller.
/// </summary>
[RequireComponent(typeof(Button))]
public class UIUserButtonBase : MonoBehaviour
{
    public Button UnityButton { get; private set; }

    private void Awake()
    {
        UnityButton = GetComponent<Button>();
        UIUserInputController.SubscribeUserButton(UnityButton);
    }
}
