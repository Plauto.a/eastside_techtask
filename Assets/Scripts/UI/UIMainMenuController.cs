﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Show or hide the main menu accoring with the current game state.
/// </summary>
public class UIMainMenuController : UIPanelBase
{
    void Awake()
    {
        GameFlowController.GameStateChanged += GameFlowController_StateChangedHandler;
    }

    private void GameFlowController_StateChangedHandler(eGameState newGameState)
    {
        switch (newGameState)
        {
            case eGameState.MainMenu:
                StartCoroutine(Show());
                break;
            default:
                StartCoroutine(Hide());
                break;
        }
    }

    private void OnDestroy()
    {
        GameFlowController.GameStateChanged -= GameFlowController_StateChangedHandler;
    }
}
