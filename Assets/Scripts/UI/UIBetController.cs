﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Maintain the Bet panel updated and only visible on the Competitive mode
/// </summary>
public class UIBetController : UIPanelBase
{
    [SerializeField]
    protected TextMeshProUGUI currentBet;

    private void Awake()
    {
        GameplayControllerBase.BetUpdated += GameplayControllerBase_BetUpdated;
        GameFlowController.GameStateChanged += GameFlowController_StateChangedHandler;
    }

    private void GameplayControllerBase_BetUpdated()
    {
        currentBet.SetText(DataManager.Instance.CurrentPlayerData.CurrentBet.ToString());
    }

    private void GameFlowController_StateChangedHandler(eGameState newGameState)
    {
        switch (newGameState)
        {
            case eGameState.CompeteMode:
                StartCoroutine(Show());
                break;
            default:
                StartCoroutine(Hide());
                break;
        }
    }

    private void OnDestroy()
    {
        GameplayControllerBase.BetUpdated -= GameplayControllerBase_BetUpdated;
        GameFlowController.GameStateChanged -= GameFlowController_StateChangedHandler;
    }
}
