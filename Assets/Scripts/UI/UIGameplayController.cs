﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Gameplay ui controller. Responsible to building and maintaing the state of the UI while both Practice and Competitive game being played
/// </summary>
public class UIGameplayController : UIPanelBase
{
    [SerializeField]
    protected TextMeshProUGUI playerGestureLabel;
    [SerializeField]
    protected TextMeshProUGUI opponentGestureLabel;
    [SerializeField]

    //Speed that the gesture cycle while waiting for results.
    [Range(1, 20)]
    private float anticipationSpeed;

    //Gameobject that hold all gesture buttons.
    [SerializeField]
    private RectTransform gesturesHolder;
    [SerializeField]
    private GestureButtonController gestureButtonPrefab;

    private bool anticipationActive = false;

    //reference for the current buttons so they can be disabled during anticipation.
    private List<GestureButtonController> currentButtons = new List<GestureButtonController>();

    private void Awake()
    {
        GameplayControllerBase.PlayReceived += GameplayControllerBase_PlayReceivedHandler;
        GameFlowController.GameStateChanged += GameFlowController_StateChangedHandler;
        UIUserInputController.UserButtonPressed += UIUserInputController_UserButtonPressedHandler;
    }

    protected void UIUserInputController_UserButtonPressedHandler(string buttonID)
    {
        if (DataManager.Instance.AvailableGestures.ContainsKey(buttonID) && !anticipationActive)
        {
            playerGestureLabel.color = Color.yellow;
            opponentGestureLabel.color = Color.yellow;
            anticipationActive = true;
            currentButtons.ForEach((button) => button.UnityButton.interactable = false);
            StartCoroutine(AnticipationEffect());
        }
    }

    private void GameplayControllerBase_PlayReceivedHandler(PlayData playData)
    {
        anticipationActive = false;
        playerGestureLabel.SetText(playData.PlayerGestureID);
        opponentGestureLabel.SetText(playData.OpponentGestureID);

        if (playData.Result > 0)
        {
            playerGestureLabel.color = Color.green;
            opponentGestureLabel.color = Color.red;
        }
        else if (playData.Result < 0)
        {
            playerGestureLabel.color = Color.red;
            opponentGestureLabel.color = Color.green;
        }
        else
        {
            playerGestureLabel.color = Color.yellow;
            opponentGestureLabel.color = Color.yellow;
        }

        currentButtons.ForEach((button) => button.UnityButton.interactable = true);
    }

    private void GameFlowController_StateChangedHandler(eGameState newGameState)
    {
        switch (newGameState)
        {
            case eGameState.PracticeMode:
            case eGameState.CompeteMode:
                StartCoroutine(Show());
                break;
            default:
                StartCoroutine(Hide());
                break;
        }
    }

    public override IEnumerator Show()
    {
        playerGestureLabel.color = Color.yellow;
        opponentGestureLabel.color = Color.yellow;
        ClearPreviousData();
        foreach (var gesture in DataManager.Instance?.CurrentGameData?.HandGestures)
        {
            GestureButtonController aGesture = Instantiate(gestureButtonPrefab, gesturesHolder);
            aGesture.Init(gesture.GestureID);
            currentButtons.Add(aGesture);
        }
        yield return StartCoroutine(base.Show());
    }

    /// <summary>
    ///Every time we show the gameplat UI, we make sure it is clean in case backend sent new data.
    /// </summary>
    private void ClearPreviousData()
    {
        currentButtons.Clear();
        int childs = gesturesHolder.childCount;
        for (int i = childs - 1; i >= 0; i--)
        {
            GameObject.Destroy(gesturesHolder.GetChild(i).gameObject);
        }
    }

    /// <summary>
    /// A Very basic anticipation effect that cycle the options until the backend sends the actual results.
    /// </summary>
    /// <returns></returns>
    private IEnumerator AnticipationEffect()
    {
        int playerIndex = 0;
        int opponentIndex = 1;
        List<GestureData> gestures = DataManager.Instance.CurrentGameData.HandGestures;
        while (anticipationActive)
        {
            GestureData gesturePlayer = gestures[playerIndex];
            playerGestureLabel.SetText(gesturePlayer.GestureID);

            GestureData gestureOpponent = gestures[opponentIndex];
            opponentGestureLabel.SetText(gestureOpponent.GestureID);

            playerIndex = (playerIndex + 1) % gestures.Count;
            opponentIndex = (opponentIndex + 1) % gestures.Count;

            yield return new WaitForSeconds(1/anticipationSpeed);
        }
    }

    private void OnDestroy()
    {
        GameplayControllerBase.PlayReceived -= GameplayControllerBase_PlayReceivedHandler;
        GameFlowController.GameStateChanged -= GameFlowController_StateChangedHandler;
        UIUserInputController.UserButtonPressed -= UIUserInputController_UserButtonPressedHandler;
    }
}
