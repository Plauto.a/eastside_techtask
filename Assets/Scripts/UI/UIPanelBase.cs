﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// UI Canvas/Panel base class, comes with a canvas group based SHOW and HIDE functionality that is a Enumarator so it can easily support
/// fades etc. More elaborate versions can receive OnComplete actions for synchronization.
/// </summary>
public class UIPanelBase : MonoBehaviour
{
    [SerializeField]
    protected CanvasGroup canvasGroup;

    public virtual IEnumerator Show()
    {
        canvasGroup.alpha = 1;
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
        yield break;
    }

    public virtual IEnumerator Hide()
    {
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
        yield break;
    }
}
