﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// THis is a helper static class  is used to provide a commom entry point for every button that signs to it.
/// Any class interested in receiving user button interactions can listen to UserButtonPressed event and check it by button name.
/// </summary>
public static class UIUserInputController
{
    public static event Action<string> UserButtonPressed = delegate { };

    public static void SubscribeUserButton(Button button)
    {
        button.onClick.AddListener(() => { ButtonPressedHandler(button.name); });
    }

    private static void ButtonPressedHandler(string buttonName)
    {
        UserButtonPressed(buttonName);
    }
}