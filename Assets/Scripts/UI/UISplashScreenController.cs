﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Splash screen, visible while loading, hiden any other time (using a fade as example of custom Hide)
/// </summary>
public class UISplashScreenController : UIPanelBase
{
    void Awake()
    {
        GameFlowController.GameStateChanged += GameFlowController_StateChangedHandler;
    }

    private void GameFlowController_StateChangedHandler(eGameState newGameState)
    {
        switch (newGameState)
        {
            case eGameState.Loading:
                StartCoroutine(Show());
                break;
            default:
                StartCoroutine(Hide());
                break;
        }
    }

    public override IEnumerator Hide()
    {
        while(canvasGroup.alpha > 0)
        {
            canvasGroup.alpha -= .05f;
            yield return new WaitForSeconds(.025f);
        }
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
    }

    private void OnDestroy()
    {
        GameFlowController.GameStateChanged -= GameFlowController_StateChangedHandler;
    }
}
