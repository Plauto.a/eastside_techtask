﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// UI panel responsable to keep the User information on the top left corner updated and in sync with backend.
/// </summary>
public class UIPlayerInfoController : UIPanelBase
{
    [SerializeField]
    private TextMeshProUGUI playerID;
    [SerializeField]
    private TextMeshProUGUI coins;

    void Awake()
    {
        GameFlowController.GameStateChanged += GameFlowController_StateChangedHandler;
        GameplayControllerBase.PlayReceived += GameplayControllerBase_PlayReceived;
    }

    private void GameFlowController_StateChangedHandler(eGameState newGameState)
    {
        switch (newGameState)
        {
            case eGameState.CompeteMode:
            case eGameState.MainMenu:
                StartCoroutine(Show());
                break;
            default:
                StartCoroutine(Hide());
                break;
        }
    }

    public override IEnumerator Show()
    {
        playerID.SetText(DataManager.Instance.CurrentPlayerData.PlayerID);
        coins.SetText(DataManager.Instance.CurrentPlayerData.Coins.ToString());
        yield return StartCoroutine(base.Show());
    }

    private void GameplayControllerBase_PlayReceived(PlayData playData)
    {
        coins.SetText(playData.PlayerCoins.ToString());
    }

    private void OnDestroy()
    {
        GameFlowController.GameStateChanged -= GameFlowController_StateChangedHandler;
        GameplayControllerBase.PlayReceived -= GameplayControllerBase_PlayReceived;
    }
}
