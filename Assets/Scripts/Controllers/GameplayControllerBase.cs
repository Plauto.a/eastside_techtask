﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Base gameplay class, provide some static events important for informing UI of imnportant events.
/// </summary>
public abstract class GameplayControllerBase : MonoBehaviour
{
    public static event Action BetUpdated = delegate { };
    public static event Action<PlayData> PlayReceived = delegate { };

    protected virtual void OnEnable()
    {
        UIUserInputController.UserButtonPressed += UIUserInputController_UserButtonPressedHandler;
    }

    protected virtual void UIUserInputController_UserButtonPressedHandler(string gestureDone)
    {

    }

    protected virtual void PlayReceivedInternal(PlayData playData)
    {
        PlayReceived(playData);
    }

    protected virtual void BetChangedInternal(int newBet)
    {
        DataManager.Instance.CurrentPlayerData.CurrentBet = newBet;
        BetUpdated();
    }

    protected virtual void OnDisable()
    {
        UIUserInputController.UserButtonPressed -= UIUserInputController_UserButtonPressedHandler;
    }
}
