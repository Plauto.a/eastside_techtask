﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


/// <summary>
/// Responsible for initializing and setting the game in a ready state.
/// Control the main game flow of the game, update and propagate the current state of the application so every listener can react accordingly.
/// </summary>
public class GameFlowController : MonoBehaviour
{
    [SerializeField]
    private float simulateLongLoadingTime = .5f;

    public static event Action<eGameState> GameStateChanged = delegate { };

    private eGameState gameState = eGameState.Undefined;

    private void Awake()
    {
        UIUserInputController.UserButtonPressed += UIUserInputController_UserButtonPressedHandler;
    }

    private IEnumerator Start()
    {
        SetGameState(eGameState.Loading);
        yield return new WaitUntil( () => DataManager.Instance.CurrentGameData != null);
        yield return new WaitUntil(() => DataManager.Instance.CurrentPlayerData != null);
        AsyncOperation loadingMainScene = SceneManager.LoadSceneAsync("MainScene", LoadSceneMode.Additive);
        yield return new WaitUntil( () => loadingMainScene.isDone);
        yield return new WaitForSeconds(simulateLongLoadingTime); //Just to simulate some long loading time.
        GameStateChanged(eGameState.MainMenu);
    }

    private void UIUserInputController_UserButtonPressedHandler(string buttonName)
    {
        //Would use some Button type here to be easyer to refactor if needed;
        switch (buttonName)
        {
            case "PracticeButton":
                SetGameState(eGameState.PracticeMode);
                break;
            case "CompeteButton":
                SetGameState(eGameState.CompeteMode);
                break;
            case "BackToMainMenu":
                SetGameState(eGameState.MainMenu);
                break;
            default:
                break;
        }
    }

    private void SetGameState(eGameState newGameState)
    {
        if (gameState == newGameState)
        {
            return;
        }
        gameState = newGameState;
        GameStateChanged(gameState);
    }

    private void OnDestroy()
    {
        UIUserInputController.UserButtonPressed -= UIUserInputController_UserButtonPressedHandler;
    }
}