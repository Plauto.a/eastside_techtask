﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Controls the Competitive version of the game
/// </summary>
public class CompeteGameplayController : GameplayControllerBase
{
    [SerializeField]
    private int currentBet = 10;
    [SerializeField]
    private int betStep = 1;

    private void Awake()
    {
        GameFlowController.GameStateChanged += GameFlowController_StateChangedHandler;
    }

    protected override void UIUserInputController_UserButtonPressedHandler(string buttonID)
    {
        //Check if the button interacted with is a bet related button, act on it.
        switch (buttonID)
        {
            case "BetDownButton":
                UpdateBet(eBetChange.Down);
                return;
            case "BetUpButton":
                UpdateBet(eBetChange.Up);
                return;
            default:
                break;
        }

        //Check of the bet set is valid 
        if (currentBet > DataManager.Instance.CurrentPlayerData.Coins)
        {
            currentBet = DataManager.Instance.CurrentPlayerData.Coins;
            BetChangedInternal(currentBet);
        }

        //Check if the button is a gesture button. If yes, act on it.
        if (DataManager.Instance.AvailableGestures.ContainsKey(buttonID))
        {
            EvaluatePlay(buttonID);
            return;
        }
    }

    /// <summary>
    /// Update bet depending on button pressed.
    /// </summary>
    /// <param name="betChange"></param>
    private void UpdateBet(eBetChange betChange)
    {
        switch (betChange)
        {
            case eBetChange.Up:
                currentBet += betStep;
                break;
            case eBetChange.Down:
                currentBet = Math.Max(currentBet - betStep, 1);
                break;
            default:
                break;
        }
        BetChangedInternal(currentBet);
    }

    private void GameFlowController_StateChangedHandler(eGameState newGameState)
    {
        switch (newGameState)
        {
            case eGameState.CompeteMode:
                gameObject.SetActive(true);
                break;
            default:
                gameObject.SetActive(false);
                break;
        }
    }

    /// <summary>
    /// For the Competitive game, the Evaluate Plays will actually send a request to backend so they play can be evaluated,
    /// </summary>
    /// <param name="gestureDone"></param>
    private void EvaluatePlay(string gestureDone)
    {
        List<GestureData> avaliableGestures = DataManager.Instance.CurrentGameData.HandGestures;
        if (avaliableGestures?.Count == 0)
        {
            return;
        }

        string playerID = DataManager.Instance.CurrentPlayerData.PlayerID;
        GestureData playerGesture = DataManager.Instance.AvailableGestures[gestureDone];

        PlayRequestData playRequestData = new PlayRequestData(playerID, playerGesture.GestureID, currentBet);

        DataManager.Instance.RequestPlay(
                        playRequestData,
                        (data) => PlayDataReceivedHandler(data), 
                        (error) => Debug.Log(error)
                        );
    }

    private void PlayDataReceivedHandler(PlayData playData)
    {
        PlayReceivedInternal(playData);
    }

    private void OnDestroy()
    {
        GameFlowController.GameStateChanged -= GameFlowController_StateChangedHandler;
    }
}
