﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This subclass of the user button used for gestures also updates the button textmesh pro text to match the gesture name.
/// </summary>
public class GestureButtonController : UIUserButtonBase
{
    public string GestureID { get; private set; }

    [SerializeField]
    private TextMeshProUGUI buttonNameLabel;

    public void Init(string gestureID)
    {
        GestureID = gestureID;
        buttonNameLabel.SetText(gestureID);
        gameObject.name = gestureID;
    }
}
