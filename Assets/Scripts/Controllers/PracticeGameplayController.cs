﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Practice mode, all is local, including the evaluation of the results
/// No bet allowed on this mode
/// </summary>
public class PracticeGameplayController : GameplayControllerBase
{
    private void Awake()
    {
        GameFlowController.GameStateChanged += GameFlowController_StateChangedHandler;
    }

    protected override void UIUserInputController_UserButtonPressedHandler(string gestureDone)
    {
        if (DataManager.Instance.AvailableGestures.ContainsKey(gestureDone))
        {
            EvaluatePlay(gestureDone);
        }
    }

    private void GameFlowController_StateChangedHandler(eGameState newGameState)
    {
        switch (newGameState)
        {
            case eGameState.PracticeMode:
                gameObject.SetActive(true);
                break;
            default:
                gameObject.SetActive(false);
                break;
        }
    }

    private void EvaluatePlay(string gestureDone)
    {
        List<GestureData> avaliableGestures = DataManager.Instance.CurrentGameData.HandGestures;
        if (avaliableGestures?.Count == 0)
        {
            return;
        }

        GestureData opponentGesture = avaliableGestures[UnityEngine.Random.Range(0, avaliableGestures.Count)];
        GestureData playerGesture = DataManager.Instance?.AvailableGestures[gestureDone];

        int result = 0; //draw
        if (playerGesture.WinsVs.Contains(opponentGesture.GestureID))
        {
            result = 1; //win
        }
        else if (playerGesture.LosesVs.Contains(opponentGesture.GestureID))
        {
            result = -1;//loss
        }

        PlayData practicePlayData = new PlayData(playerGesture.GestureID,opponentGesture.GestureID, result);
        PlayReceivedInternal(practicePlayData);
    }

    private void OnDestroy()
    {
        GameFlowController.GameStateChanged -= GameFlowController_StateChangedHandler;
    }
}
