﻿using UnityEngine;

/// <summary>
/// Very Basic Singleton base class, I avoid using them, but on some occasions it makes sense.
/// </summary>
/// <typeparam name="T"></typeparam>
public class Singleton<T>: MonoBehaviour where T: class{

	public static T Instance { get; private set; }

    protected virtual void Awake()
    {
        if (Instance != null && Instance != this as T)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this as T;
        }
    }

}