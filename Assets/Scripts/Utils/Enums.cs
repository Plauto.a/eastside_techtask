﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// I like to group all enums in a single location. Also like using a prefix 'e'
/// </summary>
public enum eGameState
{
    Undefined,
    Loading,
    MainMenu,
    PracticeMode,
    CompeteMode
}

public enum eBetChange
{
    Up,
    Down
}
