﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This class Mocks a remote server, storing data on the Player Prefs
/// </summary>
public class MockServer : MonoBehaviour, IServerInterface
{
    [SerializeField]
    private float initialLoadRemoteDataDelay = 1.5f;
    [SerializeField]
    private float playResquestResponseDelay = 1.5f;

    /// <summary>
    /// On Load, get data for the game, in this case, a list of possible Gestures players can meke. Very easy to modify and expand on the server side.
    /// </summary>
    /// <param name="OnSuccess"></param>
    /// <param name="OnError"></param>
    /// <returns></returns>
    public IEnumerator RequestGameData(Action<GameData> OnSuccess, Action<string> OnError)
    {
        yield return new WaitForSeconds(initialLoadRemoteDataDelay); //Simulate a slow server response

        List<GestureData> handGestures = new List<GestureData>()
        {
            new GestureData ("Rock", new List<string>() { "Scissors", "Nothing"}, new List<string>() { "Paper" } ),
            new GestureData ("Paper", new List<string>() { "Rock", "Nothing" }, new List<string>() { "Scissors" } ),
            new GestureData ("Scissors", new List<string>() { "Paper", "Nothing" }, new List<string>() { "Rock" } ),
            new GestureData ("Nothing", new List<string>() { }, new List<string>() {"Rock", "Paper", "Scissors" } )
        };

        GameData mockData = new GameData(handGestures);

        OnSuccess(mockData);
    }

    /// <summary>
    /// Gets the player data for the player ID.
    /// </summary>
    /// <param name="playerID"></param>
    /// <param name="OnSuccess"></param>
    /// <param name="OnError"></param>
    /// <returns></returns>
    public IEnumerator RequestPlayerData(string playerID, Action<PlayerData> OnSuccess, Action<string> OnError)
    {
        //Super simplified database Mock using local PlayerPrefs
        PlayerData storedPlayerData = null;
        if (PlayerPrefs.HasKey(playerID))
        {
            storedPlayerData = new PlayerData(playerID, PlayerPrefs.GetInt(playerID), 10);
        }
        else
        {
            storedPlayerData = new PlayerData(playerID, 10000, 10);
            PlayerPrefs.SetInt(playerID, 10000);
        }
        OnSuccess(storedPlayerData);
        yield break;
    }

    /// <summary>
    /// Return a play from server when on compentitive mode.
    /// </summary>
    /// <param name="playRequestData"></param>
    /// <param name="OnSuccess"></param>
    /// <param name="OnError"></param>
    /// <returns></returns>
    public IEnumerator RequestPlay(PlayRequestData playRequestData, Action<PlayData> OnSuccess, Action<string> OnError)
    {
        int playerCoins = PlayerPrefs.GetInt(playRequestData.PlayerID, 0);

        //Return error to client if not enough coins for the requested bet.
        if (playerCoins < playRequestData.PlayerBet)
        {
            OnError("Not enought coins to bet");
            yield break;
        }

        //Generates the random results
        List<GestureData> avaliableGestures = DataManager.Instance.CurrentGameData.HandGestures;

        GestureData opponentGesture = avaliableGestures[UnityEngine.Random.Range(0, avaliableGestures.Count)];
        GestureData playerGesture = DataManager.Instance?.AvailableGestures[playRequestData.PlayerGestureID];

        int result = 0; //draw
        if (playerGesture.WinsVs.Contains(opponentGesture.GestureID))
        {
            playerCoins += playRequestData.PlayerBet;
            result = 1; //win
        }
        else if (playerGesture.LosesVs.Contains(opponentGesture.GestureID))
        {
            playerCoins = Mathf.Max(playerCoins - playRequestData.PlayerBet, 0);
            result = -1;//loss
        }

        //Update the user backend coin amount
        PlayerPrefs.SetInt(playRequestData.PlayerID, playerCoins);

        PlayData serverSidePlayData = new PlayData(playerGesture.GestureID, playerCoins, opponentGesture.GestureID, 0, result);

        //Fake a delay on server response
        yield return new WaitForSeconds(playResquestResponseDelay);
        OnSuccess(serverSidePlayData);
    }
}
 