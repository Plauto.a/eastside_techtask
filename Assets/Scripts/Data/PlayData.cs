﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


/// <summary>
/// Data structure that holds a play RESULT data;
/// </summary>
[Serializable]
public class PlayData
{
    public string PlayerGestureID { get; private set; }
    public int PlayerCoins { get; private set; }
    public string OpponentGestureID { get; private set; }
    public int Result { get; private set; }

    public PlayData(string playerGestureID, int playerCoins, string opponentGestureID, int opponentCoins, int result)
    {
        PlayerGestureID = playerGestureID;
        PlayerCoins = playerCoins;
        OpponentGestureID = opponentGestureID;
        Result = result;
    }

    public PlayData(string playerGestureID, string opponentGestureID, int result)
    {
        PlayerGestureID = playerGestureID;
        OpponentGestureID = opponentGestureID;
        Result = result;
    }
}