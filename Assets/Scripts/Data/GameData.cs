﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// General game data needs to initialize, basic the avaliable gestures players can do.
/// </summary>
[Serializable]
public class GameData 
{
    public List<GestureData> HandGestures { get; private set; } = new List<GestureData>();

    public GameData(List<GestureData> handGestures)
    {
        HandGestures = handGestures;
    }
}
