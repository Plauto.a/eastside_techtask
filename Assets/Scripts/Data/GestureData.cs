﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Data structure that holds a single gesture information
/// </summary>
public class GestureData
{
    public string GestureID { get; private set; }
    public string GestureName { get; private set; }
    public List<string> WinsVs { get; private set; } = new List<string>();
    public List<string> LosesVs { get; private set; } = new List<string>();

    public GestureData(string gestureID, List<string> winsVs, List<string> losesVs)
    {
        GestureID = gestureID;
        GestureName = gestureID;
        WinsVs = winsVs;
        LosesVs = losesVs;
    }
}
