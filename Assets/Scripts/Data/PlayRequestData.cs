﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Data structure that holds a play resquest for backend.
/// </summary>
[Serializable]
public class PlayRequestData
{
    public string PlayerID { get; private set; }
    public string PlayerGestureID { get; private set; }
    public int PlayerBet { get; private set; }

    public PlayRequestData(string playerID, string playerGestureID, int playerBet)
    {
        PlayerID = playerID;
        PlayerGestureID = playerGestureID;
        PlayerBet = playerBet;
    }
}
