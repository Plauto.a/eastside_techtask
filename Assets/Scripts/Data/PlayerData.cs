﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


/// <summary>
/// Data structure that holds player data
/// </summary>
[Serializable]
public class PlayerData
{
    public string PlayerID {get; private set;}
    public int Coins { get; set; }
    public int CurrentBet { get; set; }

    public PlayerData(string playerID, int coins, int currentBet)
    {
        PlayerID = playerID;
        Coins = coins;
        CurrentBet = currentBet;
    }
}
